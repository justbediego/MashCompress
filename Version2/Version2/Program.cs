﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Version2
{
    public class MyTree
    {
        public class TreeNode
        {
            public int Father = -1;
            public int Son = -1;
            public int Brother = -1;
            public int DataPosition = -1;
            public char Char;
            public int Repetition = 1;
            public int Level = 0;
            public double CompressionLevel
            {
                get
                {
                    return (Repetition - 1) * Level;
                }
            }
        }
        public List<TreeNode> TreeNodes = new List<TreeNode>() { new TreeNode() };
    }
    public class Key
    {
        public string Text;
        public double Occurance = 0;
        public string BitKey;
    }
    public class HuffmanNode
    {
        public double Probabilty;
        public int KeyIndex;
        public HuffmanNode LeftChild;
        public HuffmanNode RightChild;
        public string BitCode;
    }
    class Program
    {
        static byte[] GetBytes(string bitString)
        {
            for (int i = bitString.Length; i < Math.Ceiling((double)bitString.Length / 8) * 8; i++)
            {
                bitString += "0";
            }
            byte[] output = new byte[bitString.Length / 8];

            for (int i = 0; i < output.Length; i++)
            {
                for (int b = 0; b <= 7; b++)
                {
                    output[i] |= (byte)((bitString[i * 8 + b] == '1' ? 1 : 0) << (7 - b));
                }
            }

            return output;
        }
        static void StartMoving(int dataPosition, string data, int treePosition, MyTree tree, int[] flags)
        {
            MyTree.TreeNode node = null;
            while (dataPosition < data.Length)
            {
                if (flags[dataPosition] != 0)
                    return;
                node = tree.TreeNodes[treePosition];
                if (node.Son == -1)
                {
                    node.Son = tree.TreeNodes.Count;
                    tree.TreeNodes.Add(new MyTree.TreeNode()
                    {
                        Father = treePosition,
                        Char = data[dataPosition],
                        DataPosition = dataPosition,
                        Level = node.Level + 1,
                    });
                    return;
                }
                node = tree.TreeNodes[node.Son];
                bool found = false;
                while (node.Brother != -1)
                {
                    int currentPosition = node.Brother;
                    node = tree.TreeNodes[currentPosition];
                    if (node.Char == data[dataPosition])
                    {
                        found = true;
                        if (node.Repetition == 1)
                        {
                            //call last
                            StartMoving(node.DataPosition + 1, data, currentPosition, tree, flags);
                        }
                        node.Repetition++;
                        dataPosition++;
                        treePosition = currentPosition;
                        break;
                    }
                }
                if (!found)
                {
                    node.Brother = tree.TreeNodes.Count;
                    tree.TreeNodes.Add(new MyTree.TreeNode()
                    {
                        Father = node.Father,
                        Char = data[dataPosition],
                        DataPosition = dataPosition,
                        Level = node.Level,
                    });
                    return;
                }
            }
            node.DataPosition = -1;
        }
        static void Main(string[] args)
        {
            string output = string.Empty;
            var dataBytes = File.ReadAllBytes("Test4.txt");
            var dataStr = System.Text.Encoding.Default.GetString(dataBytes);
            var dataFlags = new int[dataStr.Length];
            List<Key> keys = new List<Key>();
            List<int> finalData = new List<int>();
            while (dataFlags.Any(f => f == 0))
            {
                //create tree
                MyTree duplicationTree = new MyTree();
                for (int i = 0; i < dataStr.Length; i++)
                    if (dataFlags[i] == 0)
                        StartMoving(i, dataStr, 0, duplicationTree, dataFlags);
                //find best
                var max = duplicationTree.TreeNodes.Skip(1).OrderByDescending(t => t.CompressionLevel).First();
                //create key
                var key = new Key()
                {
                    Text = string.Empty,
                    Occurance = max.Repetition,
                };
                do
                {
                    key.Text = max.Char + key.Text;
                    max = duplicationTree.TreeNodes[max.Father];
                } while (max.Father != -1);
                keys.Add(key);
                //reduce key
                for (int i = 0; i < dataStr.Length - key.Text.Length + 1; i++)
                {
                    if (dataStr.Substring(i, key.Text.Length) == key.Text)
                    {
                        for (int j = i; j < i + key.Text.Length; j++)
                        {
                            dataFlags[j] = keys.Count;
                        }
                        i += key.Text.Length - 1;
                    }
                }
            }
            //create final data
            for (int i = 0; i < dataFlags.Length; i++)
            {
                var key = keys[dataFlags[i] - 1];
                finalData.Add(dataFlags[i] - 1);
                i += key.Text.Length - 1;
            }
            //Lets go huffman
            double compressionSum = keys.Sum(k => k.Occurance);
            List<HuffmanNode> queue = new List<HuffmanNode>();
            for (int i = 0; i < keys.Count; i++)
            {
                queue.Add(new HuffmanNode()
                {
                    KeyIndex = i,
                    Probabilty = keys[i].Occurance / compressionSum,
                });
            }
            while (queue.Count > 1)
            {
                var a = queue.OrderBy(q => q.Probabilty).First();
                queue.Remove(a);
                var b = queue.OrderBy(q => q.Probabilty).First();
                queue.Remove(b);
                HuffmanNode c = new HuffmanNode()
                {
                    KeyIndex = -1,
                    LeftChild = a,
                    RightChild = b,
                    Probabilty = a.Probabilty + b.Probabilty,
                };
                queue.Add(c);
            }
            while (queue.Count > 0)
            {
                var a = queue[0];
                queue.Remove(a);
                if (a.KeyIndex != -1)
                {
                    keys[a.KeyIndex].BitKey = a.BitCode;
                }
                if (a.LeftChild != null)
                {
                    a.LeftChild.BitCode = a.BitCode + "0";
                    queue.Add(a.LeftChild);
                }
                if (a.RightChild != null)
                {
                    a.RightChild.BitCode = a.BitCode + "1";
                    queue.Add(a.RightChild);
                }
            }
            //now lets save this shit
            List<byte> finalbytes = new List<byte>();
            foreach (var key in keys)
            {
                //size of key
                finalbytes.AddRange(BitConverter.GetBytes((short)key.Text.Length));
                finalbytes.AddRange(BitConverter.GetBytes((short)key.BitKey.Length));
                finalbytes.AddRange(Encoding.ASCII.GetBytes(key.Text));
                finalbytes.AddRange(Encoding.ASCII.GetBytes(key.BitKey));
            }
            string tmp = string.Empty;
            foreach (var data in finalData)
                tmp += keys[data].BitKey;
            finalbytes.AddRange(GetBytes(tmp));
            File.WriteAllBytes("Test4.mash", finalbytes.ToArray());
        }
    }
}
