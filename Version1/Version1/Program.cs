﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Version1
{
    class Program
    {
        class KeyOffset
        {
            public class KeyPart
            {
                public bool isEscape;
                public int EscapeSize;
                public byte Letter;
            }
            // flags : 1 removed, 0 not removed
            // Escape character \
            // \[500] 500 space
            // \\ character \
            // \; end of key
            //public string Key;
            public List<KeyPart> KeyParts;
            public double CompressionRate;
            public int StorageSize
            {
                get
                {
                    return StorageString.Length;
                }
            }
            public int CompressingSize
            {
                get
                {
                    return KeyParts.Where(k => !k.isEscape).Count();
                }
            }
            public KeyOffset(bool Initial)
            {
                KeyParts = new List<KeyPart>();
                if (Initial)
                    KeyParts.Add(new KeyPart() { isEscape = false, Letter = (byte)rand.Next(256) });
            }
            public string StorageString
            {
                get
                {
                    string output = string.Empty;
                    foreach (var item in KeyParts)
                        if (!item.isEscape)
                            if (item.Letter == '\\')
                                output += "\\\\";
                            else
                                output += (char)item.Letter;
                        else
                            output += "\\[" + item.EscapeSize.ToString() + "]";
                    return output + "\\;";
                }
            }
        }
        static Random rand = new Random();
        static int GenerationSize = 50;
        static int MutationSize = 30;
        static int ChildrenSize = 15;
        static int IterationCount = 10000;
        static double CalculateCompressionRate(byte[] data, ref bool[] flags, KeyOffset key)
        {
            int numberOfOccurances = 0;
            for (int i = 0; i < data.Length; i++)
            {
                bool found = true;
                int offset = 0;
                for (int j = 0; j < key.KeyParts.Count; j++)
                {
                    if (!key.KeyParts[j].isEscape)
                    {
                        int index = offset + j + i;
                        if (index >= flags.Length || flags[index] || data[index] != key.KeyParts[j].Letter)
                        {
                            found = false;
                            break;
                        }
                    }
                    else
                        offset += key.KeyParts[j].EscapeSize - 1; //j itself
                }
                if (found)
                {
                    i += offset + key.KeyParts.Count;
                    numberOfOccurances++;
                }
            }
            return (numberOfOccurances * key.CompressingSize) - key.StorageSize;
        }
        static KeyOffset MutateKey(KeyOffset key, int maximumJumpSize)
        {
            var mutatedBaby = new KeyOffset(false);
            //duplicating
            foreach (var item in key.KeyParts)
            {
                mutatedBaby.KeyParts.Add(new KeyOffset.KeyPart()
                {
                    EscapeSize = item.EscapeSize,
                    isEscape = item.isEscape,
                    Letter = item.Letter,
                });
            }
            var index = rand.Next(mutatedBaby.KeyParts.Count + 1);
            if (index == 0)
            {
                var tmp = GetRandomKeyPart(true, maximumJumpSize);
                mutatedBaby.KeyParts.Insert(0, tmp);
                if (tmp.isEscape)
                    mutatedBaby.KeyParts.Insert(0, GetRandomKeyPart(false, 0));
            }
            else if (index == mutatedBaby.KeyParts.Count)
            {
                var tmp = GetRandomKeyPart(true, maximumJumpSize);
                mutatedBaby.KeyParts.Add(tmp);
                if (tmp.isEscape)
                    mutatedBaby.KeyParts.Add(GetRandomKeyPart(false, 0));
            }
            else
            {
                mutatedBaby.KeyParts[index] = GetRandomKeyPart(true, 1);
            }
            //begin
            while (mutatedBaby.KeyParts[0].isEscape)
                mutatedBaby.KeyParts.RemoveAt(0);
            //end
            while (mutatedBaby.KeyParts[mutatedBaby.KeyParts.Count - 1].isEscape)
                mutatedBaby.KeyParts.RemoveAt(mutatedBaby.KeyParts.Count - 1);
            //middle
            for (int i = 1; i < mutatedBaby.KeyParts.Count - 1; i++)
            {
                if (mutatedBaby.KeyParts[i].isEscape && mutatedBaby.KeyParts[i + 1].isEscape)
                {
                    mutatedBaby.KeyParts[i].EscapeSize += mutatedBaby.KeyParts[i + 1].EscapeSize;
                    mutatedBaby.KeyParts.RemoveAt(i + 1);
                    i--;
                }
            }
            return mutatedBaby;
        }
        static KeyOffset CrossoverKey(KeyOffset key1, KeyOffset key2)
        {
            return new KeyOffset(false);
        }
        static KeyOffset.KeyPart GetRandomKeyPart(bool canJump, int jumpSize)
        {
            int b = rand.Next(256 + (canJump ? 1 : 0));
            if (b == 256)
                return new KeyOffset.KeyPart()
                {
                    isEscape = true,
                    EscapeSize = rand.Next(1, jumpSize)
                };
            else
                return new KeyOffset.KeyPart()
                {
                    isEscape = false,
                    Letter = (byte)b
                };
        }
        static int GetLongestKeySize(bool[] flags)
        {
            int longestWord = 0;
            int wordBegin = 0;
            for (int i = 0; i <= flags.Length; i++)
            {
                if (i == flags.Length || flags[i])
                {
                    int wordCount = i - wordBegin;
                    wordBegin = i + 1;
                    if (wordCount > longestWord)
                        longestWord = wordCount;
                }
            }
            return (int)Math.Ceiling((double)longestWord / 2);
        }
        static KeyOffset FindBestKey(byte[] data, ref bool[] flags)
        {
            int longestKeySize = GetLongestKeySize(flags);
            List<KeyOffset> keyOffsets = new List<KeyOffset>();

            //Initialize Generation
            for (int i = 0; i < GenerationSize; i++)
                keyOffsets.Add(new KeyOffset(true));

            //Life
            for (int it = 0; it < IterationCount; it++)
            {
                //Fitness Function
                for (int i = 0; i < GenerationSize; i++)
                    keyOffsets[i].CompressionRate = CalculateCompressionRate(data, ref flags, keyOffsets[i]);
                //Mutation
                keyOffsets.Sort(delegate (KeyOffset x, KeyOffset y) { return y.CompressionRate.CompareTo(x.CompressionRate); });
                for (int i = 0; i < MutationSize; i++)
                    keyOffsets.Add(MutateKey(keyOffsets[i], longestKeySize));
                //Crossover
                //keyOffsets.Sort(delegate (KeyOffset x, KeyOffset y) { double r = rand.NextDouble(); return r < .33 ? -1 : r < .66 ? 0 : 1; });
                //for (int i = 0; i < ChildrenSize; i++)
                //    keyOffsets.Add(CrossoverKey(keyOffsets[i], keyOffsets[GenerationSize - i]));
                //Fitness Function
                for (int i = 0; i < GenerationSize; i++)
                    keyOffsets[i].CompressionRate = CalculateCompressionRate(data, ref flags, keyOffsets[i]);
                //Killing
                keyOffsets.Sort(delegate (KeyOffset x, KeyOffset y) { return y.CompressionRate.CompareTo(x.CompressionRate); });
                keyOffsets.RemoveRange(GenerationSize, keyOffsets.Count - GenerationSize);
                Console.WriteLine(keyOffsets.First().CompressionRate + ":" + keyOffsets.First().StorageString);
            }
            return keyOffsets.First();
        }
        static void Main(string[] args)
        {
            var data = File.ReadAllBytes("Test4.txt");
            //Lets Begin
            bool[] flags = new bool[data.Length];
            List<KeyOffset> keys = new List<KeyOffset>();
            while (flags.Any(f => !f))
            {
                keys.Add(FindBestKey(data, ref flags));
            }
        }
    }
}
